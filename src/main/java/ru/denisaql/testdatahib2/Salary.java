package ru.denisaql.testdatahib2;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "\"salary\"")
@IdClass(SalaryId.class)
public class Salary {
    @Id
    @Column(name = "USERID")
    public Long userId;

    @Id
    @Column(name = "MONTH")
    public Integer month;

    @Column(name = "AMOUNT")
    public BigDecimal amount;
}
