package ru.denisaql.testdatahib2;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Embeddable;
import java.io.Serializable;

@EqualsAndHashCode
@Data
@Embeddable
public class SalaryId implements Serializable {
    Long userId;
    Integer month;
}
