package ru.denisaql.testdatahib2;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

@Entity
@Table(name = "\"user\"")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true)
    public Long id;

    @Column(name = "name")
    public String name;

    @Column(name = "details")
    public String details;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name="USERID")
    public List<Salary> salaryList;
}
