package ru.denisaql.testdatahib2;

import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Optional;

@Service
public class AppService implements ApplicationListener<ApplicationReadyEvent> {
    private UserRepository userRepository;
    private SalaryRepository salaryRepository;

    public AppService(UserRepository userRepository,
                      SalaryRepository salaryRepository) {
        this.userRepository = userRepository;
        this.salaryRepository = salaryRepository;
    }

    public void run() {
        Optional<User> user = userRepository.findById(2L);
        System.out.println(user);

        User newUser = new User();
        newUser.name = "new user!";
        newUser.details = "test details";

        userRepository.save(newUser);

        Salary salary1 = new Salary();
        salary1.userId = newUser.id;
        salary1.month = 1;
        salary1.amount = BigDecimal.valueOf(123.121);

        Salary salary2 = new Salary();
        salary2.userId = newUser.id;
        salary2.month = 2;
        salary2.amount = BigDecimal.valueOf(153.121);

        salaryRepository.save(salary1);
        salaryRepository.save(salary2);
        salaryRepository.flush();

        Optional<User> res = userRepository.findById(newUser.id);
        System.out.println(res);
    }

    @Override
    public void onApplicationEvent(ApplicationReadyEvent event) {
        run();
    }
}
